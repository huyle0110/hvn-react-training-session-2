import React from "react";
import {Link} from "react-router-dom";

const Header = () => {
  return (
    <header className="header">
      <nav className="navbar navbar-expand-lg">
        <div className="search-area">
          <div className="search-area-inner d-flex align-items-center justify-content-center">
            <div className="close-btn"><i className="icon-close"/></div>
            <div className="row d-flex justify-content-center">
              <div className="col-md-8">
                <form action="#">
                  <div className="form-group">
                    <input type="search" name="search" id="search" placeholder="What are you looking for?"/>
                    <button type="submit" className="submit">
                      <i className="icon-search-1"/>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="navbar-header d-flex align-items-center justify-content-between">
            <Link to="index.html" className="navbar-brand">Bootstrap Blog</Link>
            <button type="button" data-toggle="collapse" data-target="#navbarcollapse" aria-controls="navbarcollapse"
                    aria-expanded="false" aria-label="Toggle navigation" className="navbar-toggler">
              <span/><span/><span/>
            </button>
          </div>
          <div id="navbarcollapse" className="collapse navbar-collapse">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to="index.html" className="nav-link active ">Home</Link>
              </li>
              <li className="nav-item">
                <Link to="blog.html" className="nav-link ">Blog</Link>
              </li>
              <li className="nav-item">
                <Link to="post.html" className="nav-link ">Post</Link>
              </li>
              <li className="nav-item">
                <Link to="#" className="nav-link ">Contact</Link>
              </li>
            </ul>
            <div className="navbar-text">
              <Link to="#" className="search-btn">
                <i className="icon-search-1"/>
              </Link>
            </div>
            <ul className="langs navbar-text">
              <Link to="#" className="active">EN</Link>
              <span/>
              <Link to="#">ES</Link>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
};

export default Header;
